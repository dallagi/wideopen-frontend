# WideOpen Dashboard  (aka _that_ thing that killed rush hours) 🔪 🚙 🚗 ⛔️

A simple dashboard for the [WideOpen project](https://hackathon.bz.it/project/wideopen).


It shows the pattern of entrances and exits from the parking lot of the company using the data collected from IoT sensors.
It also shows the amount of traffic in the city of Bolzano/Bozen (averaged over the last few years) aggregated by the time of the day, in order to allow the company to compare it to the patterns of travel to/from the company offices of their employees.


## Wanna help? Project setup! 💚

Since we want to be able to extend and improve the dashboard in a modular way we have choosen to use [VueJS](https://github.com/vuejs/vue) (we are sure you love it too ).

Start right away installing it along the other dependencies!

```
npm install
```

### Compiles and hot-reloads for development  🚀
```
npm run serve
```

### Compiles and minifies for production 🚢
```
npm run build
```

### Run your tests 🏥
```
npm run test
```

### Lints and fixes files. (When the code gives you  🍋)
```
npm run lint
```
